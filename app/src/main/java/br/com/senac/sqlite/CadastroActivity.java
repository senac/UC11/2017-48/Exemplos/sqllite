package br.com.senac.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CadastroActivity extends AppCompatActivity {

    private EditText txtNome ;
    private EditText txtTelefone ;
    private EditText txtEmail ;
    private EditText txtSite ;
    private Button btnSalvar ;

    private Aluno aluno ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        /* buscando componentes na view */
        txtNome = findViewById(R.id.txtNome);
        txtTelefone = findViewById(R.id.txtTelefone);
        txtEmail = findViewById(R.id.txtEmail);
        txtSite = findViewById(R.id.txtSite);
        btnSalvar = findViewById(R.id.btnSalvar);

        /* Adicionando acao de click no botao  */
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aluno = new Aluno();

                aluno.setNome(txtNome.getText().toString());
                aluno.setTelefone(txtTelefone.getText().toString());
                aluno.setEmail(txtEmail.getText().toString());
                aluno.setSite(txtSite.getText().toString());
                
                //salvar no banco
            }
        });

    }
}
