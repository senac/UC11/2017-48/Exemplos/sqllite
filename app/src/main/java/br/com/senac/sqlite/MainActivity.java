package br.com.senac.sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listViewAlunos ;
    private ArrayAdapter<Aluno> adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* buscando elementos na view */
        listViewAlunos = findViewById(R.id.listViewAlunos);

        /* criando e populando lista */
        Aluno aluno1 = new Aluno("Jose da Silva" ,"(27)9999-9999" , "jose@jose.com.br" , "www.senac.com.br");

        Aluno aluno2 = new Aluno("Manuel Santos" ,"(27)9999-9999" , "manuel@manuel.com.br" , "www.senac.com.br");

        Aluno aluno3 = new Aluno("jonas Carvalho" ,"(27)9999-9999" , "jonas@jonas.com.br" , "www.senac.com.br");

        List<Aluno> lista = new ArrayList<>() ;

        lista.add(aluno1);
        lista.add(aluno2);
        lista.add(aluno3);

        /* criando adapter */

        adapter = new ArrayAdapter<Aluno>(this , android.R.layout.simple_list_item_1 , lista) ;

        /* definindo o adapter do listview */

        listViewAlunos.setAdapter(adapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;
    }

    public void adicionar(MenuItem item){

        Intent intent = new Intent(this , CadastroActivity.class);
        startActivity(intent);

    }
}
